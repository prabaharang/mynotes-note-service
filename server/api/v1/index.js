const router = require('express').Router();

const notes = require('./notes/notes.route');

router.use('/notes', notes);

module.exports = router;
