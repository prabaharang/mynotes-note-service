const db = require('mongoose');
const Schema = db.Schema;

const NoteSchema = new Schema({
  id:  String,
  title: String,
  text: String,
  state: {
    type: String,
    default: 'not-started'
  },
  userId: String,
  createdOn: {
    type: Date,
    default: new Date()
  },
  modifiedOn: {
    type: Date
  },
  favourite: {
    type: Boolean
  },
  group: {
    type: String
  },
  reminder: {
    type: String
  },
  sharedWith: {
    type: Array
  }
});

const NoteModel = db.model('Note', NoteSchema);

module.exports = NoteModel;
