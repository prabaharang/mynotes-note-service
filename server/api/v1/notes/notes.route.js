const request = require('request-promise-native');
const router = require('express').Router();

const JSONStream = require('JSONStream');

const NoteService = require('./notes.service');

router.use(async (req, res, next) => {
  const Authorization = req.get('Authorization');
  if (!Authorization) res.status(403).send('Not authenticated');
  else {
    try {
      let response = await request.post(
        `${process.env.USER_API}/auth`,
        { headers: { Authorization }, json: true }
      );
      req.user = response.user;
      next();
    } catch (err) {
      res.status(403).send(err);
    }
  }
});

router.route('/').post(async (req, res) => {
  let note;
  if (req.body && req.user) {
    req.body.userId = req.user.userId;
  }
  note = await NoteService.save(req.body);
  res.status(201).json(note);
});

router.route('/:noteId').put(async (req, res) => {
  let note;
  if (req.body && req.user) {
    req.body.userId = req.user.userId;
  }
  await NoteService.update(req.params.noteId, req.body);
  note = await NoteService.getById(req.params.noteId);
  res.status(200).json(note);
});

router.route('/favourite/:noteIds').put(async (req, res) => {
  const noteIds = req.params.noteIds.split(',');
  const request = { noteIds, userId: req.user.userId };
  let response;
  
  try {
    response = await NoteService.updateFavourite(request);
    res.status(200).json({ message: `${response.updatedCount} note(s) marked as favourites` });
  } catch (err) {
    res.status(500).json({ message: `Error marking note(s) as favourites` });
  }
});

router.route('/share/:email').post(async (req, res) => {
  const email = req.params.email;
  const userId = req.user.userId;
  const noteIds = req.body.noteIds;
  const accessType = req.body.accessType;

  const userInfo = { userId, email };
  const request = { userInfo, noteIds, accessType };

  try {
    await NoteService.updateSharing(request);
    res.status(200).json({ message: 'Note(s) shared successfully' });
  } catch (err) {
    res.status(500).json({ message: 'Error sharing note(s)' });
  }
});

router.route('/group/:noteIds/:groupName').put(async (req, res) => {
  const noteIds = req.params.noteIds.split(',');
  const groupName = req.params.groupName;
  const userId = req.user.userId;
  const request = { noteIds, groupName, userId };
  let response;
  
  try {
    response = await NoteService.updateGroup(request);
    res.status(200).json({ message: `${response.updatedCount} note(s) grouped to ${groupName}` });
  } catch (err) {
    res.status(500).json({ message: `Error grouping notes` });
  }
});

router.route('/').get(async (req, res) => {
  const notes = await NoteService.get(req.user);
  res.status(200).json(notes);
});

router.route('/search').get(async (req, res) => {
  let criteria = { };
  if (req.query.title) {
    criteria.title = req.query.title;
  }
  const notes = await NoteService.search(req.user, criteria);
  if (notes.length) {
    res.status(200).json(notes);
  } else {
    res.status(404).json({ message: 'No results found' });
  }
});

router.route('/:noteIds').delete(async (req, res) => {
  const noteIds = req.params.noteIds.split(',');
  const deleteRequest = { noteIds };
  let deleteResponse;
  if (req.user) {
    deleteRequest.userId = req.user.userId;
  }
  try {
    deleteResponse = await NoteService.delete(deleteRequest);
    res.status(200).json({ message: `${deleteResponse.deletedCount} note(s) deleted successfully` });
  } catch (err) {
    res.status(500).json({ message: 'Error deleting note(s)' });
  }
});

router.route('/stream').post((req, res) => {
  NoteService.bulkSave(() => res.status(200).send('Documents created successfully'));
});

router.route('/stream').get((req, res) => {
  NoteService.bulkGet()
    .pipe(JSONStream.stringify())
    .pipe(res.type('json'));
});

module.exports = router;
