const NoteModel = require('./notes.model');
const uuid = require('uuid/v1');

const streamToMongoDB = require('stream-to-mongo-db').streamToMongoDB;
const JSONStream = require('JSONStream');

const NoteDAO = { };

const sharedSearchClause = user => ({
  $or: [
    { userId: user.userId },
    { sharedWith: { $elemMatch: { email: user.email } } }
  ]
});

NoteDAO.save = async noteData => {
  noteData.id = uuid();
  const note = new NoteModel(noteData);
  return await note.save();
};

NoteDAO.get = async user => {
  return await NoteModel.find(sharedSearchClause(user));
};

NoteDAO.getById = async id => {
  return await NoteModel.findOne({ id });
};

NoteDAO.search = async (user, criteria) => {
  return await NoteModel.find({
    $and: [
      { title: { $regex: new RegExp(criteria.title, 'i') } },
      sharedSearchClause(user)
    ]
  });
};

NoteDAO.update = async (id, noteData) => {
  const $set = {
    title: noteData.title,
    text: noteData.text,
    state: noteData.state,
    group: noteData.group,
    reminder: noteData.reminder
  };

  return await NoteModel.updateOne(
    { id },
    { $set },
    { upsert: true }
  );
};

NoteDAO.updateFavourite = async request => {
  return await NoteModel.updateMany(
    { id: { $in: request.noteIds }, userId: request.userId },
    { $set: { favourite: true } },
    { upsert: true }
  );
};

NoteDAO.updateSharing = async (id, userInfo, accessType) => {
  const note = await NoteDAO.getById(id);
  const shareInfo = { email: userInfo.email, accessType };
  const shared = note.sharedWith.find(shared => shared.email === userInfo.email);

  if (!shared) {
    note.sharedWith.push(shareInfo);
  } else {
    Object.assign(shared, shareInfo);
  }

  await NoteModel.updateOne(
    { id, userId: userInfo.userId },
    { $set: { sharedWith: note.sharedWith } },
    { upsert: true }
  );
};

NoteDAO.updateGroup = async request => {
  return await NoteModel.updateMany(
    { id: { $in: request.noteIds }, userId: request.userId },
    { $set: { group: request.groupName } },
    { upsert: true }
  );
};

NoteDAO.delete = async deleteRequest => {
  return await NoteModel.deleteMany({ id: { $in: deleteRequest.noteIds }, userId: deleteRequest.userId });
};

NoteDAO.streamToDB = (stream, cb) => {
  const writableStream = streamToMongoDB({
    dbURL: process.env.MONGO_URL,
    collection: 'notes'
  });

  stream
    .pipe(JSONStream.parse('*'))
    .pipe(writableStream)
    .on('finish', cb);
};

NoteDAO.streamFromDB = () => NoteModel.find({ }).stream();

module.exports = NoteDAO;
