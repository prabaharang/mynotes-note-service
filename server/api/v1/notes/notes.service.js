const fs = require('fs');
const path = require('path');
const NoteDAO = require('./notes.dao');

const NoteService = { };

NoteService.save = async note => {
  const _note = await NoteDAO.save(note);
  delete _note._id;
  return _note;
};

NoteService.get = async user => {
  return await NoteDAO.get(user);
};

NoteService.getById = async id => {
  return await NoteDAO.getById(id);
};

NoteService.search = async (user, criteria) => {
  return await NoteDAO.search(user, criteria);
};

NoteService.update = async (noteId, note) => {
  const response = await NoteDAO.update(noteId, note);
  return { updatedCount: response.nModified };
};

NoteService.updateFavourite = async request => {
  const response = await NoteDAO.updateFavourite(request);
  return { updatedCount: response.nModified };
};

NoteService.updateSharing = async request => {
  const updates = request.noteIds
    .map(noteId => NoteDAO.updateSharing(noteId, request.userInfo, request.accessType));
  await Promise.all(updates);
};

NoteService.updateGroup = async request => {
  const response = await NoteDAO.updateGroup(request);
  return { updatedCount: response.nModified };
};

NoteService.delete = async deleteRequest => {
  let deleteResponse;
  if (!deleteRequest.noteIds.length) {
    throw Error('DELETE_ERROR');
  }
  deleteResponse = await NoteDAO.delete(deleteRequest);
  return { deletedCount: deleteResponse.n };
};

NoteService.bulkSave = cb => {
  const inputFilePath = path.join(__dirname, '../../../mock_notes.json');
  const readStream = fs.createReadStream(inputFilePath);
  NoteDAO.streamToDB(readStream, cb);
};

NoteService.bulkGet = () => NoteDAO.streamFromDB();

module.exports = NoteService;
