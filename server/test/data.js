module.exports = {
  allNotes: [
    {
      userId: 'user1',
      id: '1',
      title: 'title11',
      text: 'text11'
    },
    {
      userId: 'user1',
      id: '2',
      title: 'title12',
      text: 'text12'
    },
    {
      userId: 'user1',
      id: '3',
      title: 'title13',
      text: 'text13'
    },
    {
      userId: 'user2',
      id: '4',
      title: 'title21',
      text: 'text21'
    }
  ],
  deleteNote: [
    {
      title: 'title1-to-delete',
      text: 'text1-to-delete',
      state: 'state1-to-delete',
      userId: 'user1',
      id: 'del1'
    },
    // title2, title3 for user2 to test multiple note deletion
    {
      title: 'title2-to-delete',
      text: 'text2-to-delete',
      state: 'state2-to-delete',
      userId: 'user2',
      id: 'del2'
    },
    {
      title: 'title3-to-delete',
      text: 'text3-to-delete',
      state: 'state3-to-delete',
      userId: 'user2',
      id: 'del3'
    },
    {
      title: 'title4-to-delete',
      text: 'text4-to-delete',
      state: 'state4-to-delete',
      userId: 'user4',
      id: 'del4'
    }
  ],
  favouriteNotes: [
    {
      title: 'title1-favourite',
      text: 'text1-favourite',
      state: 'state1-favourite',
      userId: 'user1',
      id: 'fav1'
    },
    // title2, title3 for user2 to test multiple note updation
    {
      title: 'title2-favourite',
      text: 'text2-favourite',
      state: 'state2-favourite',
      userId: 'user2',
      id: 'fav2'
    },
    {
      title: 'title3-favourite',
      text: 'text3-favourite',
      state: 'state3-favourite',
      userId: 'user2',
      id: 'fav3'
    },
    {
      title: 'title4-favourite',
      text: 'text4-favourite',
      state: 'state4-favourite',
      userId: 'user4',
      id: 'fav4'
    }
  ],
  groupNotes: [
    {
      title: 'title1-group',
      text: 'text1-group',
      state: 'state1-group',
      userId: 'user1',
      id: 'id1'
    },
    // title2, title3 for user2 to test multiple note updation
    {
      title: 'title2-group',
      text: 'text2-group',
      state: 'state2-group',
      userId: 'user2',
      id: 'id2'
    },
    {
      title: 'title3-group',
      text: 'text3-group',
      state: 'state3-group',
      userId: 'user2',
      id: 'id3'
    },
    {
      title: 'title4-group',
      text: 'text4-group',
      state: 'state4-group',
      userId: 'user4',
      id: 'id4'
    }
  ],
  searchNotes: [
    {
      title: 'title1-search',
      text: 'text1-search',
      state: 'state1-search',
      userId: 'user1',
      id: 'search1'
    },
    {
      title: 'similar-title1-search',
      text: 'text1-search',
      state: 'state1-search',
      userId: 'user1',
      id: 'search1'
    },
    {
      title: 'title2-search',
      text: 'text2-search',
      state: 'state2-search',
      userId: 'user2',
      id: 'search2'
    }
  ],
  shareNotes: [
    {
      title: 'title1-share',
      text: 'text1-share',
      state: 'state1-share',
      userId: 'user1',
      id: 'share1'
    },
    {
      title: 'title2-share',
      text: 'text2-share',
      state: 'state2-share',
      userId: 'user1',
      id: 'share2'
    },
    {
      title: 'title3-share',
      text: 'text3-share',
      state: 'state3-share',
      userId: 'user1',
      id: 'share3',
      sharedWith: [ { email: 'user2@mail', accessType: 'read' } ]
    },
    {
      title: 'title5-share-write',
      text: 'text5-share-write',
      state: 'state5-share-write',
      userId: 'user1',
      id: 'share5'
    },
    {
      title: 'title6-share-write',
      text: 'text6-share-write',
      state: 'state6-share-write',
      userId: 'user1',
      id: 'share6',
      sharedWith: [ { email: 'user2@mail', accessType: 'read' } ]
    },
    {
      title: 'title4-share',
      text: 'text4-share',
      state: 'state4-share',
      userId: 'user1',
      id: 'share4'
    },
    {
      title: 'title7-share',
      text: 'text7-share',
      state: 'state7-share',
      userId: 'invalid-user',
      id: 'share7'
    },
  ]
};
