const should = require('chai').should();
const nock = require('nock');
const request = require('supertest');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const data = require('./data');
const app = require('../app');

const NoteModel = require('../api/v1/notes/notes.model');

// Master test suite
describe('Note Test Suite', () => {
  let server;
  const userIds = [ 1, 2, 999 ], tokens = { };

  before(async () => {
    server = app.listen();
    await mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true });

    userIds.forEach(userId => {
      tokens[`user${userId}`] = jwt.sign(
        { userName: `username${userId}`, userId: `user${userId}` },
        '$ecret',
        { expiresIn: 86400 }
      );
    });
  });

  after(async () => {
    await NoteModel.deleteMany({ });
    mongoose.connection.close();
    server.close();
  });

  beforeEach(() => {
    userIds.forEach(userId => {
      nock(
        `${process.env.USER_API}`,
        { reqheaders: { Authorization: `Bearer ${tokens[`user${userId}`]}` } }
      ).post('/auth')
        .reply(200, { user: { userName: `username${userId}`, userId: `user${userId}` } });
    });
  });
  
  //  testsuite
  describe('Testing to add a note', () => {

    after(async () => {
      await NoteModel.deleteMany({ });
    });

    //  testcase
    it('Should handle a request to add a new note for user 1 ', async () => {
      // Should get added note of user 1 as a respone,  need to match added note text value
      // status = 201
      // response will be added note object
      // Pass valid token in Authorization header as Bearer
      // done();
      const res = await request(server)
        .post('/api/v1/notes')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send({
          title: 'title1',
          text: 'text1',
          state: 'state1'
        });

      res.status.should.equal(201, 'Status should be 201 Created');
      res.body.should.have.property('text');
      res.body.text.should.equal('text1');
      res.body.userId.should.equal('user1');
      res.body.should.have.property('id');
    });

    //  testcase
    it('Should handle a request to add a new note for user 2', async () => {
      // Should get added note of user 2 as a respone,  need to match added note text value
      // status = 201
      // response will be added note object
      // Pass valid token in Authorization header as Bearer
      // done();
      const res = await request(server)
        .post('/api/v1/notes')
        .set('Authorization', `Bearer ${tokens.user2}`)
        .send({
          title: 'title2',
          text: 'text2',
          state: 'state2'
        });

      res.status.should.equal(201, 'Status should be 201 Created');
      res.body.should.have.property('text');
      res.body.text.should.equal('text2');
      res.body.userId.should.equal('user2');
      res.body.should.have.property('id');
    });
  });

  //  testsuite
  describe('Testing to get all notes', () => {
    before(async () => NoteModel.insertMany(data.allNotes));

    //  testcase
    it('Should handle a request to get all notes for user 1', async () => {
      // Should get all note as a array those are created by user 1 and Should match recently added note text value
      // status = 200
      // response will be a array or all notes those are added by user 1
      // Pass valid token in Authorization header as Bearer
      // done();
      const res = await request(server)
        .get('/api/v1/notes')
        .set('Authorization', `Bearer ${tokens.user1}`);
      
      res.status.should.equal(200);
      res.body.should.be.instanceof(Array);
      res.body.length.should.equal(3);
    });

    //  testcase
    it('Should handle a request to get all notes for user 2', async () => {
      // Should get all note as a array those are created by user 2 and Should match recently added note text value
      // status = 200
      // response will be a array or all notes those are added by user 2
      // Pass valid token in Authorization header as Bearer
      // done();
      const res = await request(server)
        .get('/api/v1/notes')
        .set('Authorization', `Bearer ${tokens.user2}`);
      
      res.status.should.equal(200);
      res.body.should.be.instanceof(Array);
      res.body.length.should.equal(1);
    });

    //  testcase
    it('Should handle a request to get notes of a user who has not created any note', async () => {
      // should get blank array
      // status = 200
      // response will be an empty array
      // Pass valid token in Authorization header as Bearer
      // done();

      const res = await request(server)
        .get('/api/v1/notes')
        .set('Authorization', `Bearer ${tokens.user999}`);
      
      res.status.should.equal(200);
      res.body.should.be.instanceof(Array);
      res.body.length.should.equal(0);
    });
  });

  //  testsuite
  describe('Testing to update a note', () => {
    //  testcase
    it('Should handle a request to update a note by note id', async () => {
      // Should return updated note and match updated note text value'
      // status = 200
      // response will hold updated note as an object
      // Pass valid token in Authorization header as Bearer
      // done();

      const res = await request(server)
        .put('/api/v1/notes/3')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send({
          title: 'title3-updated',
          text: 'text3-updated',
          state: 'state3-updated',
          userId: 'user1',
          id: '3'
        });

      res.status.should.equal(200, 'Status should be 200 Created');
      res.body.should.have.property('text');
      res.body.text.should.equal('text3-updated');
    });
  });

  // testsuite
  describe('Testing to delete a note', () => {
    before(async () => NoteModel.insertMany(data.deleteNote));

    // test case
    it('Should delete single note by note ID', async () => {
      let note;
      const res = await request(server)
        .delete('/api/v1/notes/del1')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send();

      // DB delete verification
      note = await NoteModel.findOne({ id: 'del1', userId: 'user1'});
      should.not.exist(note);

      // response verification
      res.status.should.equal(200);
      res.body.message.should.equal('1 note(s) deleted successfully');
    });

    // test case
    it('Should delete multiple notes by note ID', async () => {
      let notes;
      const res = await request(server)
        .delete('/api/v1/notes/del2,del3')
        .set('Authorization', `Bearer ${tokens.user2}`)
        .send();

      // DB delete verification
      notes = await NoteModel.find({ id: { $in: [ 'del2', 'del3' ] } });
      notes.length.should.equal(0, '2 notes should be deleted and return empty');

      // response verification
      res.status.should.equal(200);
      res.body.message.should.equal('2 note(s) deleted successfully');
    });
  });

  // testsuite
  describe('Testing to mark notes as favourites', () => {
    before(async () => NoteModel.insertMany(data.favouriteNotes));

    // test case
    it('Should mark single note as favourite by note ID', async () => {
      let note;
      const res = await request(server)
        .put('/api/v1/notes/favourite/fav1')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send();

      // DB verification
      note = await NoteModel.findOne({ id: 'fav1', userId: 'user1'});
      note.should.have.property('favourite');
      note.favourite.should.equal(true);

      // response verification
      res.status.should.equal(200);
      res.body.message.should.equal('1 note(s) marked as favourites');
    });

    // test case
    it('Should mark multiple notes as favourite by note ID', async () => {
      let notes;
      const res = await request(server)
        .put('/api/v1/notes/favourite/fav2,fav3')
        .set('Authorization', `Bearer ${tokens.user2}`)
        .send();

      // DB delete verification
      notes = await NoteModel.find({ id: { $in: [ 'fav2', 'fav3' ] } });
      notes[0].should.have.property('favourite');
      notes[0].favourite.should.equal(true);
      notes[1].should.have.property('favourite');
      notes[1].favourite.should.equal(true);

      // response verification
      res.status.should.equal(200);
      res.body.message.should.equal('2 note(s) marked as favourites');
    });
  });

  // testsuite
  describe('Testing to group notes', () => {
    before(async () => NoteModel.insertMany(data.groupNotes));

    // test case
    it('Should add single note to group', async () => {
      let note;
      const res = await request(server)
        .put('/api/v1/notes/group/id1/testgroup1')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send();

      // DB verification
      note = await NoteModel.findOne({ id: 'id1', userId: 'user1'});
      note.should.have.property('group');
      note.group.should.equal('testgroup1');

      // response verification
      res.status.should.equal(200);
      res.body.message.should.equal('1 note(s) grouped to testgroup1');
    });

    // test case
    it('Should add multiple notes to group', async () => {
      let notes;
      const res = await request(server)
        .put('/api/v1/notes/group/id2,id3/testgroup2')
        .set('Authorization', `Bearer ${tokens.user2}`)
        .send();

      // DB delete verification
      notes = await NoteModel.find({ id: { $in: [ 'id2', 'id3' ] } });
      notes[0].should.have.property('group');
      notes[0].group.should.equal('testgroup2');
      notes[1].should.have.property('group');
      notes[1].group.should.equal('testgroup2');

      // response verification
      res.status.should.equal(200);
      res.body.message.should.equal('2 note(s) grouped to testgroup2');
    });
  });

  // testsuite
  describe('Testing to share notes', () => {
    before(async () => NoteModel.insertMany(data.shareNotes));

    // test case
    it('Should share notes by email for unshared notes', async () => {
      let note;
      await request(server)
        .post('/api/v1/notes/share/user2@mail')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send({ noteIds: [ 'share1', 'share2' ] });

      // DB verification
      note = await NoteModel.findOne({ id: 'share1' });
      note.should.have.property('sharedWith');
      note.sharedWith.length.should.be.greaterThan(0);
      note.sharedWith[0].should.have.property('email');
      note.sharedWith[0].email.should.equal('user2@mail');

      note = await NoteModel.findOne({ id: 'share2' });
      note.should.have.property('sharedWith');
      note.sharedWith[0].should.have.property('email');
      note.sharedWith[0].email.should.equal('user2@mail');
    });

    // test case
    it('Should share notes by email for already shared note', async () => {
      let note;

      await request(server)
        .post('/api/v1/notes/share/user3@mail')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send({ noteIds: [ 'share3', 'share4' ] });

      // DB verification
      note = await NoteModel.findOne({ id: 'share3' });
      note.should.have.property('sharedWith');
      note.sharedWith.length.should.be.greaterThan(0);
      note.sharedWith[1].should.have.property('email');
      note.sharedWith[1].email.should.equal('user3@mail');

      note = await NoteModel.findOne({ id: 'share4' });
      note.should.have.property('sharedWith');
      note.sharedWith.length.should.be.greaterThan(0);
      note.sharedWith[0].should.have.property('email');
      note.sharedWith[0].email.should.equal('user3@mail');
    });

    // test case
    it('Should share notes by email with write access', async () => {
      let note;

      await request(server)
        .post('/api/v1/notes/share/user3@mail')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send({ noteIds: [ 'share5', 'share6' ], accessType: 'write' });

      // DB verification
      note = await NoteModel.findOne({ id: 'share5' });
      note.should.have.property('sharedWith');
      note.sharedWith.length.should.be.greaterThan(0);
      note.sharedWith[0].should.have.property('email');
      note.sharedWith[0].email.should.equal('user3@mail');
      note.sharedWith[0].should.have.property('accessType');
      note.sharedWith[0].accessType.should.equal('write');

      note = await NoteModel.findOne({ id: 'share6' });
      note.should.have.property('sharedWith');
      note.sharedWith.length.should.be.greaterThan(0);
      note.sharedWith[0].should.have.property('accessType');
      note.sharedWith[0].accessType.should.equal('read'); // unmodified
      note.sharedWith[1].should.have.property('email');
      note.sharedWith[1].email.should.equal('user3@mail');
      note.sharedWith[1].should.have.property('accessType');
      note.sharedWith[1].accessType.should.equal('write');
    });
  });

  // test suite
  describe('Search notes', () => {
    before(async () => NoteModel.insertMany(data.searchNotes));

    // test case
    it('Should search note by title', async () => {
      const res = await request(server)
        .get('/api/v1/notes/search?title=title1-search')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send();

      // response verification
      res.status.should.equal(200);
      res.body.length.should.equal(2);
    });
  });

  describe('Negative test scenarios', () => {
    it('Make a API request to a resource with invalid token, which requires authentication, should return forbidden status and error ', async () => { 
      const res = await request(server)
        .get('/api/v1/notes')
        .set('Authorization', 'Bearer invalid token');
      
      res.status.should.equal(403);
    });
    it('Make a API request to a resource without any token, which requires authentication, should return forbidden status and error ', async () => {
      const res = await request(server)
        .get('/api/v1/notes');
      
      res.status.should.equal(403);
    });
    // test case
    it('Should return error message when notes are searched with invalid title', async () => {
      const res = await request(server)
        .get('/api/v1/notes/search?title=title999-search-invalid')
        .set('Authorization', `Bearer ${tokens.user1}`)
        .send();

      // response verification
      res.status.should.equal(404);
      res.body.should.have.property('message');
      res.body.message.should.equal('No results found');
    });
  });

  describe('Testing streaming to MongoDB', () => {
    it('Should make API call to stream input file to MongoDB', async () => {
      const res = await request(server)
        .post('/api/v1/notes/stream')
        .set('Authorization', `Bearer ${tokens.user1}`);
      
      const notes = await NoteModel.find({ id: { $regex: /^n/ } });
      notes.length.should.be.greaterThan(1);
      
      res.status.should.equal(200, 'Status should be 200 Created');
    });

    it('Should get notes as stream', async () => {
      const res = await request(server)
        .get('/api/v1/notes/stream')
        .set('Authorization', `Bearer ${tokens.user1}`);
  
      res.body.length.should.be.greaterThan(1);
      res.status.should.equal(200, 'Status should be 200 Created');
    });
  });
});
