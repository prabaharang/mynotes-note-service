FROM node:8.12-alpine

RUN mkdir /src
WORKDIR /src
COPY server /src/server
COPY yarn.lock /src
COPY package.json /src

RUN npm install -g -s --no-progress yarn && \
    yarn && \
    yarn cache clean

CMD [ "yarn", "start" ]
